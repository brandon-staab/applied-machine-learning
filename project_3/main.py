import bayes_opt, glob, os, pathlib, sys, warnings

if not sys.warnoptions:
	warnings.simplefilter("ignore")
	os.environ["PYTHONWARNINGS"] = "ignore" # Also affect subprocesses

from sklearn import (
	datasets,
	decomposition,
	exceptions,
	metrics,
	model_selection,
	neural_network,
	preprocessing,
)

from scipy.stats import norm
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

def descriptive_statistics(dataset):
	df = pd.DataFrame(dataset.data, columns=dataset.feature_names)
	df['target'] = pd.Series(dataset.target)

	predictors_summary = df.drop(['target'], axis=1).describe().append(
		df.drop(['target'], axis=1).mode().iloc[0].rename('mode')
	).round(4)

	label_summary = pd.DataFrame([
		df['target'].value_counts(),
		(df['target'].value_counts() / df['target'].count() * 100).round(1),
	], ['#', '%'])

	print(f'Summary of Predictors:\n{predictors_summary}\n\nSummary of Labels:\n{label_summary}')


################################################################################
'''
a.     Different hidden layers (1, 2, 3)
b.     Different neurons on each layer. Here is the detail (Thanks to John):
  One Hidden Layer Models
    1)  (5)
    2)  (10)
  Two Hidden Layer Models
    3)  (5, 10)
    4)  (10, 5)
    5)  (10, 20)
  Three Hidden Layer Models
    6) (5, 10, 20)
    7) (5, 20, 10)
c. Gradient decent solvers (lbfgs, sgd, adam)
d. Activation function (logistic, tanh, relu)
e. Different regularization parameter ( 0.01, 0.1, 1)
f. Scaled data/unscaled
'''

def scale(X, scaler_idx):
	scaler, scaler_name = (
		(None, 'None'),
		(preprocessing.MaxAbsScaler, 'MinMaxScaler'),
		(preprocessing.StandardScaler, 'StandardScaler'),
		(preprocessing.RobustScaler, 'RobustScaler'),
	)[scaler_idx]

	if scaler:
		X = scaler().fit_transform(X)

	return X, scaler_name


def get_results(searcher, scaler_name):
	results = pd.DataFrame(searcher.cv_results_)
	results['scaler'] = scaler_name

	return results


def grid_search(dataset):
	results = pd.DataFrame(columns=[
		'mean_fit_time', 'std_fit_time', 'mean_score_time',	'std_score_time',
		'param_activation', 'param_alpha', 'param_hidden_layer_sizes',
		'param_max_iter', 'param_solver', 'params', 'split0_test_score',
		'split1_test_score', 'split2_test_score', 'split3_test_score',
		'split4_test_score', 'mean_test_score', 'std_test_score',
		'rank_test_score', 'scaler'])

	param_list_req = {
		'hidden_layer_sizes': (
			(5), (10), (20),
			(5, 10), (10, 5), (10, 20),
			(5, 10, 20), (5, 20, 10)
		),
		'solver': ('lbfgs', 'sgd', 'adam'),
		'activation':  ('logistic', 'tanh', 'relu'),
		'alpha': (0.01, 0.1, 1.0)
	}

	max_nodes_end = 25
	param_list_large = {
		'hidden_layer_sizes': \
			[(x) for x in range(5, max_nodes_end, 5)] + \
			[(x, y) for x in range(5, max_nodes_end, 5) for y in range(5, max_nodes_end, 5)] + \
			[(x, y, z) for x in range(5, max_nodes_end, 5) for y in range(5, max_nodes_end, 5) for z in range(5, max_nodes_end, 5)],
		'solver': ('lbfgs', 'sgd', 'adam'),
		'activation':  ('logistic', 'tanh', 'relu'),
		'alpha': sorted([round(0.5 * j * 10 ** i, 5) for i in range(-3, 3) for j in range(1, 3)]),
	}

	param_list = param_list_large
	mlp = neural_network.MLPClassifier(max_iter=500, random_state=1)
	searcher = model_selection.GridSearchCV(mlp, param_list, cv=model_selection.RepeatedStratifiedKFold(n_splits=5, n_repeats=5, random_state=1),  n_jobs=-1, verbose=2)

	for scaler_idx in range(4):
		Y = dataset.target
		X, scaler_name = scale(dataset.data, scaler_idx)
		searcher.fit(X, Y)
		results = results.append(get_results(searcher, scaler_name))

	return results.sort_values('mean_test_score')

################################################################################

def bayes_search(dataset):
	counter = 0

	def score_mlp(**kwargs):
		nonlocal counter
		get_int = lambda x: int(kwargs[x])

		counter += 1
		print(f'{counter:3}: ', end='', flush=True)

		# Create hidden layers and make sure there are no zeros in the middle or end
		hidden_layers = [get_int(key) for key in kwargs.keys() if key.startswith('layer_')]
		while hidden_layers and not hidden_layers[-1]:
			hidden_layers.pop()
		if not np.all(hidden_layers):
			return 0

		# Lookup string params
		solver = ('sgd', 'lbfgs', 'adam')[get_int('solver')]
		activation = ('tanh', 'logistic', 'relu')[get_int('activation')]
		learning_rate = ('constant', 'invscaling', 'adaptive')[get_int('learning_rate')]

		X = dataset.data
		Y = dataset.target

		# Rotate and scale dataset
		pca_components = get_int('pca_components')
		if pca_components:
			pca = decomposition.PCA(pca_components)
			X = pca.fit_transform(X)
		X, scaler_name = scale(X, get_int('scaler_idx'))

		# Make MLP classifer
		print(f'{hidden_layers=}, {activation=}, {solver=}, {learning_rate=}, {pca_components=}, {scaler_name=}', end='', flush=True)
		mlp = neural_network.MLPClassifier(hidden_layers, activation, solver, max_iter=1000, learning_rate=learning_rate)

		# Cross validate
		cv_scores = model_selection.cross_validate(mlp, X, Y, n_jobs=2,
			cv=model_selection.RepeatedStratifiedKFold(n_splits=10, n_repeats=10, random_state=1))

		# Find confidence interval
		test_scores = cv_scores['test_score']
		mu_hat = np.average(test_scores)
		sigma_hat = np.std(test_scores)
		ci = norm.interval(0.68, mu_hat, sigma_hat)
		print(f'-> ci={np.round(ci, 5) * 100}, {len(test_scores)}')

		# Save good models
		if mu_hat > 0.99:
			print(f'*** mu = {mu * 100:6.5f} ***')
			pd.DataFrame(cv_scores).to_csv('good_models.csv', mode='a', header=False)

		return ci[0]


	param_bounds = {
		'layer_1': (1, 257),
		'layer_2': (0, 129),
		'layer_3': (0, 65),
		'layer_4': (0, 33),
		'pca_components': (0, len(dataset.feature_names) + 0.9999),
		'solver': (0, 2.9999),
		'activation': (0, 2.9999),
		'alpha': (1e-4, 1e4),
		'scaler_idx': (0, 3.9999),
		'learning_rate': (0, 2.9999)
	}

	path='./logs'
	pathlib.Path(path).mkdir(parents=True, exist_ok=True)
	optimizer = bayes_opt.BayesianOptimization(score_mlp, param_bounds)
	bayes_opt.util.load_logs(optimizer, logs=glob.glob(f'{path}/*')[-10:])
	print(f'Optimizer loaded {len(optimizer.space)} points.')

	optimizer.subscribe(bayes_opt.Events.OPTIMIZATION_STEP, bayes_opt.logger.JSONLogger(path=pd.Timestamp('today').strftime(f'{path}/%Y-%m-%d_%H%-M-%S.json')))
	#optimizer.subscribe(bayes_opt.Event.OPTIMIZATION_STEP, bayes_opt.logger.ScreenLogger())
	optimizer.maximize(init_points=5, n_iter=25)
	print(f'Max: {optimizer.max}')

################################################################################

def plot_model_confusion_matrix(model, X, Y, classes, title):
	matrix = np.zeros( (len(classes), len(classes)) )
	for i in range(1):
		y_pred = model_selection.cross_val_predict(model, X, Y, cv=model_selection.StratifiedKFold(5, random_state=1), n_jobs=-1)
		matrix += metrics.confusion_matrix(
			y_true=Y,
			y_pred=y_pred,
			labels=classes
		)

	matrix /= matrix.sum(axis=1)
	metrics.ConfusionMatrixDisplay(matrix, display_labels=classes).plot()
	plt.suptitle(f'{title} Confusion Matrix')
	plt.show()


def make_confusion_matrices(dataset):
	classes = [False, True]
	title = 'MLP'
	X = dataset.data
	Y = dataset.target
	model = neural_network.MLPClassifier()

	plot_model_confusion_matrix(model, X, Y, classes, title)


################################################################################

def main():
	dataset = datasets.load_breast_cancer()

	if 'y' == input('Show data stats? [y/N]').lower():
		descriptive_statistics(dataset)

	if 'y' == input('Do grid search? [y/N]').lower():
		grid_search(dataset).to_csv('results1.csv')

	if 'y' == input('Do bayes search? [y/N]').lower():
		for x in range(100):
			bayes_search(dataset)

	if 'y' == input('Make confusion matrices? [y/N]').lower():
		make_confusion_matrices(dataset)

if __name__ == '__main__':
        main()

