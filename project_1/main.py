from matplotlib import cm
from mpl_toolkits.mplot3d import axes3d
from pandas.plotting import scatter_matrix
from sklearn.metrics import plot_confusion_matrix, confusion_matrix
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.neighbors import KNeighborsClassifier
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from datasets import *


X = data_set[features]
Y = data_set[label]

samples_per_run = 5000
max_neighbors = 25
max_p = 25


if 'y' == input("Data summary? [Y/N]").lower():
    summary = X.agg(['mean', 'std', 'median', 'min', 'max'])
    mode = X.mode().iloc[0].rename('mode')
    print('Features:\n', summary.append(mode).round(2))

    print('\nClasses:\n', pd.DataFrame([
        Y.value_counts(),
        (Y.value_counts() / Y.count() * 100).round(1),
    ], ['#', '%']))


if 'y' == input("Quick 80/20 matrix? [Y/N]").lower():
    print('K =', k)
    print('P =', p)

    knn = KNeighborsClassifier(n_neighbors=k)
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y)
    knn.fit(X_train, Y_train)
    plot_confusion_matrix(knn, X_test, Y_test, normalize='true')
    plt.show()


if 'y' == input("Quick 80/20 average? [Y/N]").lower():
    print('K = ', k)
    print('P = ', p)
    print('Features = ', features)
    knn = KNeighborsClassifier(n_neighbors=k)
    scores = []
    for j in range(samples_per_run):
        X_train, X_test, Y_train, Y_test = train_test_split(X, Y)
        knn.fit(X_train, Y_train)
        scores.append(knn.score(X_test, Y_test))
    print(f'Accuracy: {100 * np.mean(scores): .2f}%')


if 'y' == input("Show data plot? [Y/N]").lower():
    # Generate colors
    labels = Y.unique()
    colors = {x:y for x,y in zip(labels, range(len(labels)))}
    Y_colors = pd.Series(colors[key] for key in Y)

    # Plotting a scatter matrix
    cmap = cm.get_cmap('gnuplot')
    scatter = scatter_matrix(X, c=Y_colors, marker='o', hist_kwds={'bins':15}, figsize=(9,9), cmap=cmap)

    # Plotting a 3D scatter plot
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.set_xlabel(ploting_features_label[0])
    ax.set_ylabel(ploting_features_label[1])
    ax.set_zlabel(ploting_features_label[2])

    ax.scatter(X[ploting_features[0]], X[ploting_features[1]], X[ploting_features[2]],
        c=Y_colors, marker='o', s=100)
    plt.show()


if 'y' == input("Run accuracy test? [Y/N]").lower():
    # How sensitive is K-NN classification accuracy to the choice of the 'k' parameter?
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y)

    plt.figure()
    for k in  range(1, max_neighbors):
        print('K = ', k)
        scores = []
        for i in range(samples_per_run):
            knn = KNeighborsClassifier(n_neighbors=k, weights=weights)
            knn.fit(X_train, Y_train)
            scores.append(knn.score(X_test, Y_test))
        plt.plot(k, 100 * np.mean(scores), 'bo')

    plt.xlabel('K-neighbors')
    plt.ylabel('Accuracy (%)')
    plt.xticks([0, 5, 10, 15, 20])
    plt.show()


if 'y' == input("Run sensitivity test? [Y/N]").lower():
    # How sensitive is K-NN classification accuracy to the train/test split proportion?
    step_size = 10
    print('Step size: ', step_size)
    print('Total samples per run: ', samples_per_run)
    print('Total samples: ', step_size * samples_per_run)

    knn = KNeighborsClassifier(n_neighbors=k)
    plt.figure()
    for i in range(max(10, step_size), 100, step_size):
        print(f'{i}% Train, {100-i}% Test')
        test_size = 1 - i / 100
        scores = []
        for j in range(samples_per_run):
            X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size)
            knn.fit(X_train, Y_train)
            scores.append(knn.score(X_test, Y_test))
        plt.plot(i, 100 * np.mean(scores), 'bo')

    plt.xlabel('Training set proportion (%)')
    plt.ylabel('Accuracy (%)')
    plt.xticks([x for x in range(max(10, step_size), 100, step_size)])
    plt.show()


if 'y' == input("Do grid search? [Y/N]").lower():
    # For testing dropping features (requires indenting lines of code)
    #for f in features:
    #    a = features.copy()
    #    a.remove(f)
    #
    #    X = data_set[a]

    # Defining parameter range
    param_grid = {
        'n_neighbors': [x for x in range(1, max_neighbors)],
        'p': [x for x in range(1, max_p)],
        'weights': ['uniform', 'distance'],
    }

    # Fitting the model for grid search
    grid = GridSearchCV(KNeighborsClassifier(), param_grid, refit = True, n_jobs=-1)
    stats = {}
    for i in range(100):
        grid.fit(X, Y)
        parms = tuple(p for p in grid.best_params_.values())
        if parms in stats:
            stats[parms] += 1
        else:
            stats[parms] = 1

    summary = list(reversed(sorted([(v, k) for k, v in stats.items()])))
    #print(f)
    print('#    : ', [k for k in param_grid.keys()])
    for x, p in summary:
        print(f'{x :<4} : {p}')

