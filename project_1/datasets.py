import pandas as pd


datasets_dir = '../datasets/'

#choice = 'fruit'
#choice = 'fish'
#choice = 'leaf'
choice = 'yeast'
#choice = 'car'


if choice == 'fruit':
    k = 5
    p = 1
    weights = 'distance'
    data_set = pd.read_csv(datasets_dir + 'fruit.tsv', sep='\t')
    label = 'fruit_label'
    features = ['height', 'width', 'mass', 'color_score']
    ploting_features = ['height', 'mass', 'color_score']
    ploting_features_label = ['Height', 'Mass', 'Color']


if choice == "fish":
    # https://www.kaggle.com/aungpyaeap/fish-market
    k = 5
    p = 1
    weights = 'distance'
    data_set = pd.read_csv(datasets_dir + 'fish.csv')
    label = 'Species'
    features = ['Weight', 'Length1', 'Length2', 'Length3', 'Height', 'Width']
    ploting_features = ['Weight', 'Length3', 'Height']
    ploting_features_label = ['Weight', 'Length', 'Height']


if choice == 'leaf':
    # https://archive.ics.uci.edu/ml/datasets/Leaf
    k = 7
    p = 1
    weights = 'uniform'
    data_set = pd.read_csv(datasets_dir + 'leaf.csv')
    label = 'Species'
    features = ['Specimen',
        'Eccentricity',
        'Aspect_Ratio',
        'Elongation',
        'Solidity',
        'Stochastic_Convexity',
        'Isoperimetric_Factor',
        'Maximal_Indentation_Depth',
        'Lobedness',
        'Average_Intensity',
        'Average_Contrast',
        'Smoothness',
        'Third_moment',
        'Uniformity',
        'Entropy',
    ]
    ploting_features = ['Eccentricity', 'Elongation', 'Entropy']
    ploting_features_label = ['Eccentricity', 'Elongation', 'Entropy']


if choice == 'yeast':
    # https://archive.ics.uci.edu/ml/datasets/Yeast
    k = 19
    p = 2
    weights = 'uniform'

    #k = 24
    #p = 1
    #weights = 'distance'

    data_set = pd.read_csv(datasets_dir + 'yeast.csv')
    label = 'class'
    features = [
        # k/p/w/a
        # all: 24/1/d/58.35/58.41/58.42/58.43
        'mcg', # 24/18/d/55.92
        'gvh', # 17/2/d/56.38
        'alm', # 23/2/u/49.01
        'mit', # 20/1/d/51.76
        #'erl', # 19/2/u/58.80/58.78/58.77/58.75
        'pox', # 19/2/u/58.27
        'vac', # 24/3/u/58.22
        'nuc', # 22/1/d/53.86
    ]
    ploting_features = ['alm', 'mit', 'nuc']
    ploting_features_label = ['alm', 'mit', 'nuc']


if choice == 'car':
    # https://archive.ics.uci.edu/ml/datasets/Auto+MPG
    k = 11
    p = 1
    weights = 'distance'
    data_set = pd.read_csv(datasets_dir + 'car.csv')
    label = 'make'
    features = [
        'mpg',
        'cylinders',
        'displacement',
        'horsepower',
        'weight',
        'acceleration',
        'model_year',
        'origin',
    ]
    ploting_features = ['horsepower', 'weight', 'acceleration']
    ploting_features_label = ['Horsepower', 'Weight', 'Acceleration']


assert type(k) is int
assert type(p) is int
assert type(weights) is str
assert type(features) is list
assert type(label) is str
assert len(ploting_features) == 3
assert len(ploting_features_label) == 3
