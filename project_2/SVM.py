from score_model import score_model_classifier

from sklearn.svm import (
	LinearSVC,
	SVC,
)
import pandas as pd

#############
#    SVM    #
#############

MAX_ITER = 1e5

def show_svm_models(test_size, iterations):
	# Create classifier object: Create a linear SVM classifier
	# C: Regularization parameter. Default C=1
	lsvc = LinearSVC(C=0.02, max_iter=MAX_ITER)
	score_model_classifier(lsvc, 'Linear SVM (C=0.02)', test_size, iterations)

	#print(lsvc.coef_)
	#print(lsvc.intercept_)


	# Create classifier object: Create a nonlinear SVM classifier
	# kernel:
	#   default = ’rbf’ (radial basis function)
	#   if poly, default degree = 3
	psvc = SVC(C=2, degree=2, kernel='poly', max_iter=MAX_ITER)
	score_model_classifier(psvc, 'Polynomial SVM (C=2, d=2)', test_size, iterations)


	# Create classifier object: Create a nonlinear SVM classifier
	# kernel:
	#   radial basis function -> Gaussian
	gsvc = SVC(C=1, kernel='rbf', max_iter=MAX_ITER)
	score_model_classifier(gsvc, 'RBF SVC (C=1)', test_size, iterations)

	gsvc = SVC(C=0.2, kernel='sigmoid', max_iter=MAX_ITER)
	score_model_classifier(gsvc, 'Sigmoid SVM (C=0.2)', test_size, iterations)


def rank_svm_models(test_size, iterations):
	df = pd.DataFrame(columns=['train', 'test'])

	cs = sorted([round(0.2 * j * 10 ** i, 5) for i in range(-1, 3) for j in range(1, 6)])

	for c in cs:
		for kernel in ['linear', 'poly', 'rbf', 'sigmoid']:
			if kernel == 'poly':
				for d in range(2, 5):
					df = df.append(score_model_classifier(
						model=SVC(C=c, degree=d, kernel=kernel, max_iter=MAX_ITER),
						title=f'{kernel.capitalize()} SVC (C={c}, d={d})',
						test_size=test_size,
						iterations=iterations,
						show_matrix=False
					))
			else:
				df = df.append(score_model_classifier(
						model=SVC(C=c, kernel=kernel, max_iter=MAX_ITER),
						title=f'{kernel.capitalize()} SVC (C={c})',
						test_size=test_size,
						iterations=iterations,
						show_matrix=False
					))

	return df

