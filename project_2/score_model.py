from dataset import (X, Y)

from sklearn.exceptions import ConvergenceWarning
from sklearn.metrics import (
	confusion_matrix,
	plot_confusion_matrix,
	ConfusionMatrixDisplay,
)
from sklearn.model_selection import train_test_split

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import warnings


def display_matrix(matrix, title):
	matrix /= matrix.sum(axis=0)
	ConfusionMatrixDisplay(matrix, display_labels=['False', 'True']).plot()
	plt.suptitle(f'{title} Confusion Matrix')
	plt.show()


def score_model_classifier(model, title, test_size, iterations, show_matrix=True):
	with warnings.catch_warnings():
		warnings.filterwarnings(action='ignore', category=ConvergenceWarning)

		stats = np.zeros(2)
		matrix = np.zeros((2, 2))
		for _ in range(iterations):
			X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size)

			model.fit(X_train, Y_train)

			if show_matrix:
				Y_test_hat = model.predict(X_test)
				matrix += confusion_matrix(Y_test, Y_test_hat)

			stats[0] += model.score(X_train, Y_train)
			stats[1] += model.score(X_test, Y_test)

		stats *= 100 / iterations
		print(f"{title} training set score: {stats[0] :0.2f}%")
		print(f"{title} test set score: {stats[1] :0.2f}%\n")

		if show_matrix:
			display_matrix(matrix, title)

	return pd.DataFrame([[title, stats[0], stats[1], test_size]], columns=['title', 'train', 'test', 'test_size'])

################################################################################

def score_model_regression_bin_class(model, title, test_size, iterations, show_matrix=True):
	with warnings.catch_warnings():
		warnings.filterwarnings(action='ignore', category=ConvergenceWarning)

		stats = np.zeros(4)
		matrix = np.zeros((2, 2))
		for _ in range(iterations):
			X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size)

			model.fit(X_train, Y_train)

			Y_train_hat = model.predict(X_train) > 0.5
			Y_test_hat = model.predict(X_test) > 0.5

			if show_matrix:
				matrix += confusion_matrix(Y_test, Y_test_hat)

			correct_train = Y_train_hat == Y_train
			correct_test = Y_test_hat == Y_test

			stats[0] += correct_train.sum() / correct_train.count()
			stats[1] += correct_test.sum() / correct_test.count()

			stats[2] += model.score(X_train, Y_train)
			stats[3] += model.score(X_test, Y_test)

		stats *= 100 / iterations
		print(f"{title} training set score: {stats[0] :0.2f}%")
		print(f"{title} training set correlation coefficient: {stats[2] :0.2f}%")

		print(f"{title} test set score: {stats[1] :0.2f}%")
		print(f"{title} test set correlation coefficient: {stats[3] :0.2f}%\n")

		if show_matrix:
			display_matrix(matrix, title)


		return pd.DataFrame([[title, stats[0], stats[1], test_size]], columns=['title', 'train', 'test', 'test_size'])


def best_regression_bin_class(Model, title, alphas, test_size, iterations):
	df = pd.DataFrame(columns=['train', 'test'])
	for alpha in alphas:
		df = df.append(score_model_regression_bin_class(
			model=Model(alpha),
			title=f'{title} (alpha={alpha})',
			test_size=test_size,
			iterations=iterations,
			show_matrix=False
		))

	return df

