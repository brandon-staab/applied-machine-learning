from score_model import score_model_classifier

from sklearn.linear_model import LogisticRegression
import pandas as pd
import numpy as np


#############################
#    Logistic Regression    #
#############################

MAX_ITER = 1e5

def show_logistic_models(test_size, iterations):
	logreg = LogisticRegression(C=0.04, penalty='elasticnet', solver='saga', l1_ratio=0.8)
	score_model_classifier(logreg, 'Logistic Regression (C=0.04, ratio=0.8, Elasticnet)', test_size, iterations)

	# Coefficients of linear model (b_1,b_2,...,b_p): log(p/(1-p)) = b0+b_1x_1+b_2x_2+...+b_px_p
	print("logreg.coef_: {}".format(logreg.coef_))
	print("logreg.intercept_: {}".format(logreg.intercept_))


	'''
	# Use the trained logistic repression model to predict a new, previously unseen object
	case_to_predict = [
		...,
	]

	class_label_prediction = logreg.predict(case_to_predict)
	class_label_probability = logreg.predict_proba([case_to_predict])
	print("class_label: {}".format(class_label_prediction[0]))
	print("fail/pass probability: {}".format(class_label_probability[0]))
	'''

def rank_logistic_models(test_size, iterations):
	df = pd.DataFrame(columns=['train', 'test'])

	for c in sorted([round(0.2 * j * 10 ** i, 5) for i in range(-1, 3) for j in range(1, 6)]):
		for penalty in ['l1', 'l2', 'elasticnet']:
			if penalty == 'elasticnet':
				for l1_ratio in np.arange(0.2, 1, 0.2).round(1):
					df = df.append(score_model_classifier(
						model=LogisticRegression(C=c, penalty=penalty, max_iter=MAX_ITER, solver='saga', l1_ratio=l1_ratio),
						title=f'Logistic Regression (C={c}, ratio={l1_ratio}, {penalty.capitalize()})',
						test_size=test_size,
						iterations=iterations,
						show_matrix=False
					))
			else:
				df = df.append(score_model_classifier(
					model=LogisticRegression(C=c, penalty=penalty, max_iter=MAX_ITER, solver='saga'),
					title=f'Logistic Regression (C={c}, {penalty.capitalize()})',
					test_size=test_size,
					iterations=iterations,
					show_matrix=False
				))

	df = df.append(score_model_classifier(
		model=LogisticRegression(max_iter=MAX_ITER, solver='saga'),
		title=f'Logistic Regression',
		test_size=test_size,
		iterations=iterations,
		show_matrix=False
	))

	return df

