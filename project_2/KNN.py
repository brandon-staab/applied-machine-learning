from score_model import score_model_classifier

from sklearn.neighbors import KNeighborsClassifier
import pandas as pd

##############
#    k-NN    #
##############

def show_knn_models(test_size, iterations):
	knn = KNeighborsClassifier(n_neighbors=18, p=1, weights='distance')
	score_model_classifier(knn, 'k-NN (k=18, p=1, Distance)', test_size, iterations)


def rank_knn_models(test_size, iterations):
	df = pd.DataFrame(columns=['train', 'test'])

	for weight in ['uniform', 'distance']:
		for k in range(2, 20):
			for p in range(1, 5):
				df = df.append(score_model_classifier(
					model=KNeighborsClassifier(n_neighbors=k, weights=weight, p=p),
					title=f'k-NN (k={k}, p={p}, {weight.capitalize()})',
					test_size=test_size,
					iterations=iterations,
					show_matrix=False
				))

	return df

