from dataset import X, Y
from score_model import (
	score_model_regression_bin_class,
	best_regression_bin_class,
)

from matplotlib import pyplot as plt
from sklearn.linear_model import (
	Lasso,
	LinearRegression,
	Ridge,
)
from sklearn.model_selection import (
	LeaveOneOut,
	cross_val_predict,
	train_test_split,
)
import pandas as pd


####################################################
#    Linear Regression (Ordinary Least Squares)    #
####################################################

def show_linear_models(test_size, iterations):
	lr = LinearRegression()

	# Estimate the accuracy of the classifier on future data, using the test data
	# score = 1 - relative_score
	# R^2(y, hat{y}) = 1 - {sum_{i=1}^{n} (y_i - hat{y}_i)^2} / {sum_{i=1}^{n} (y_i - bar{y})^2}
	score_model_regression_bin_class(lr, 'Linear Regression', test_size, iterations)

	print("lr.coef_: {}".format(lr.coef_))
	print("lr.intercept_: {}".format(lr.intercept_))

	'''
	# Use the trained linear repression model to predict a new, previously unseen object
	case_to_predict = [
		...,
	]

	class_prediction = lr.predict([case_to_predict])
	print("Pass: {}".format(class_prediction[0]))
	'''

	################################################################################

	if 'y' == input("Run cross validation and leave on out on linear regression model? [y/N]").lower():
		X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.25, random_state=42)

		fig, axs = plt.subplots(1,2)

		predicted = cross_val_predict(lr, X, Y, cv=10)
		axs[0].set_title('Cross Validation')
		axs[0].scatter(Y, predicted, edgecolors=(0, 0, 0))
		axs[0].plot([Y.min(), Y.max()], [Y.min(), Y.max()], 'k--', lw=4)
		axs[0].set_xlabel('Measured')
		axs[0].set_ylabel('Predicted')
		axs[0].set_ylim(-2.0, 2.0)


		# Leave one out: Provides train/test indices to split data in train/test sets.
		# Each sample is used once as a test set (singleton) while the remaining samples
		# form the training set.  n = the number of samples
		loo = LeaveOneOut()
		loo.get_n_splits(X)

		lr = LinearRegression()
		predicted = []
		measured = []
		for train_index, test_index in loo.split(X):
			# print("TRAIN:", train_index, "TEST:", test_index)
			X_train, X_test = X.loc[train_index], X.loc[test_index]
			Y_train, Y_test = Y[train_index], Y[test_index]
			lr.fit(X_train, Y_train)
			predicted.append(lr.predict(X_test)[0])
			measured.append(Y_test.iloc[0])

		axs[1].set_title('Leave One Out')
		axs[1].scatter(measured, predicted, edgecolors=(0, 0, 0))
		axs[1].plot([min(measured), max(measured)], [min(measured), max(measured)], 'k--', lw=4)
		axs[1].set_xlabel('Measured')
		axs[1].set_ylabel('Predicted')
		axs[1].set_ylim(-2.0, 2.0)
		plt.show()


################################################################################
#   Ridge regression --- a more stable model                                   #
################################################################################
# In ridge regression, the coefficients (w) are chosen not only so that they   #
# predict well on the training data, but also to fit an additional constraint. #
# We also want the magnitude of coefficients to be as small as possible; in    #
# other words, all entries of w should be close to zero. This constraint is an #
# example of what is called regularization. Regularization means explicitly    #
# restricting a model to avoid overfitting.                                    #
#     Minimizing: ||y - Xw||^2_2 + alpha * ||w||^2_2                           #
#                                                                              #
# Note: the smaller alpha = the less restriction.                              #
################################################################################

	ridge = Ridge(alpha=10)
	score_model_regression_bin_class(ridge, 'Ridge Regression (alpha=10)', test_size, iterations)

	print("ridge.coef_: {}".format(ridge.coef_))
	print("sum ridge.coef_^2: {}".format(sum(ridge.coef_*ridge.coef_)))
	print("ridge.intercept_: {}\n\n".format(ridge.intercept_))


################################################################################
#    Lasso regression --- a more stable model                                  #
################################################################################
# In lasso regression, the coefficients (w) are chosen not only so that they   #
# predict well on the training data, but also to fit an additional constraint. #
# We also want the magnitude of coefficients to be as small as possible; in    #
# other words, all entries of w should be close to zero. This constraint is an #
# example of what is called regularization. Regularization means explicitly    #
# restricting a model to avoid overfitting.                                    #
#                                                                              #
#     Minimizing: ||y - Xw||_2 + alpha * ||w||_2                               #
#                                                                              #
# Note: the smaller alpha = the less restriction.                              #
################################################################################

	lasso = Lasso(alpha=0.06)
	score_model_regression_bin_class(lasso, 'Lasso Regression (alpha=0.06)', test_size, iterations)

	print("lasso.coef_: {}".format(lasso.coef_))
	print("sum lasso.coef_^2: {}".format(sum(lasso.coef_*lasso.coef_)))
	print("lasso.intercept_: {}\n\n".format(lasso.intercept_))


def rank_linear_models(test_size, iterations):
	df = score_model_regression_bin_class(
		model=LinearRegression(),
		title='Linear Regression',
		test_size=test_size,
		iterations=iterations,
		show_matrix=False
	)

	alphas=sorted([round(0.2 * j * 10 ** i, 5) for i in range(-5, 2) for j in range(1, 6)])
	df = df.append(best_regression_bin_class(
			Model=Ridge,
			title='Ridge Regression',
			alphas=alphas,
			iterations=iterations,
			test_size=test_size
		))

	df = df.append(best_regression_bin_class(
			Model=Lasso,
			title='Lasso Regression',
			alphas=alphas,
			iterations=iterations,
			test_size=test_size
		))

	return df

