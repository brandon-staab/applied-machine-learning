from dataset import *

from matplotlib import cm
from mpl_toolkits.mplot3d import axes3d # scatter plot: projection = '3d'
from pandas.plotting import scatter_matrix
import matplotlib as mpl
import matplotlib.pyplot as plt


def descriptive_statistics(dataset, numeric_predictors, categorical_predictors, response):
	numeric_predictors_summary = dataset[numeric_predictors].describe().append(
		dataset[numeric_predictors].mode().iloc[0].rename('mode')
	).round(2)

	response_summary = pd.DataFrame([
		dataset[response].value_counts(),
		(dataset[response].value_counts() / dataset[response].count() * 100).round(1),
	], ['#', '%'])

	print(f'''
Sample of Data Frame:
{dataset[[response] + numeric_predictors + categorical_predictors]}

Predictors:
  Numeric:
    Labels: [{", ".join(numeric_predictors)}]
    Number: {len(numeric_predictors)}
    Summary:
{numeric_predictors_summary}
    Correlation Matrix:
{dataset[numeric_predictors].corr().round(4)}
    Variance/Covariance Matrix:
{dataset[numeric_predictors].cov().round(4)}

  Categorical:
    Labels: [{", ".join(categorical_predictors)}]
    Number: {len(categorical_predictors)}
    Summary:
{dataset[categorical_predictors].describe()}

Response:
  {response}
  Summary:
{response_summary}

''')


def show_encoded(X, Y):
	print(X.join(Y))


def plot_corr_cov(df, title, fontsize):
	fig, axs = plt.subplots(1, 2, constrained_layout=True)
	fig.suptitle(title, fontsize=fontsize)

	axs[0].set_title('Correlation Matrix', fontsize=fontsize - 4);
	axs[0].matshow(df.corr())
	axs[0].set_xticks(range(len(df.columns)))
	axs[0].set_yticks(range(len(df.columns)))
	axs[0].set_xticklabels(df.columns, fontsize=fontsize - 8, rotation=45)
	axs[0].set_yticklabels(df.columns, fontsize=fontsize - 8)

	axs[1].set_title('Variance/Covariance Matrix', fontsize=fontsize - 4);
	axs[1].matshow(df.cov())
	axs[1].set_xticks(range(len(df.columns)))
	axs[1].set_yticks(range(len(df.columns)))
	axs[1].set_xticklabels(df.columns, fontsize=fontsize - 8, rotation=45)
	axs[1].set_yticklabels(df.columns, fontsize=fontsize - 8)
	plt.show()


def plot_linear(X, Y):
	plt.subplot(2, 2, 1)
	plt.scatter(X['age'], Y, marker='o', color='blue', s=12)
	plt.xlabel('age')
	plt.ylabel('G3')

	plt.subplot(2, 2, 2)
	plt.scatter(X['absences'], Y, marker='o', color='blue', s=12)
	plt.xlabel('absences')
	plt.ylabel(response)

	plt.subplot(2, 2, 3)
	plt.scatter(X['G1'], Y, marker='o', color='blue', s=12)
	plt.xlabel('G1')
	plt.ylabel(response)

	plt.subplot(2, 2, 4)
	plt.scatter(X['G2'], Y, marker='o', color='blue', s=12)
	plt.xlabel('G2')
	plt.ylabel(response)
	plt.show()


def plot_logistic(X, Y):
	x_axis_feature = 'G1'
	plt.plot(X[x_axis_feature], Y, 'ro')
	plt.ylabel(response)
	plt.xlabel(x_axis_feature)
	plt.show()


def plot_knn(X, Y):
	cmap = cm.get_cmap('gnuplot')
	scatter = scatter_matrix(X, c= Y, marker = 'o', s=15, hist_kwds={'bins':15}, figsize=(10,10), cmap=cmap)

	fig = plt.figure()
	ax = fig.add_subplot(111, projection = '3d')
	ax.scatter(X['absences'], X['G1'], X['G2'], c = Y, marker = 'o', s=100)
	ax.set_xlabel('Absences')
	ax.set_ylabel('G1')
	ax.set_zlabel('G2')
	plt.show()


def display_stats():
	if 'y' == input("Show descriptive statistics? [y/N]").lower():
		descriptive_statistics(dataset, numeric_predictors, categorical_predictors, response)

	if 'y' == input("Show encoded data? [y/N]").lower():
		show_encoded(X, Y)

	if 'y' == input("Plot correlation and variance/covariance matrix? [y/N]").lower():
		plot_corr_cov(dataset[numeric_predictors], 'Continuous Predictors', fontsize=18)

	if 'y' == input("Plot linear regression? [y/N]").lower():
		plot_linear(X, Y)

	if 'y' == input("Plot logistic regression? [y/N]").lower():
		plot_logistic(X, Y)

	if 'y' == input("Plot k-NN? [y/N]").lower():
		if 'y' == input("Plot k-NN of just numeric predictors? [y/N]").lower():
			plot_knn(X[numeric_predictors], Y)

		if 'y' == input("Plot k-NN of numeric and categorical predictors (takes a long time)? [y/N]").lower():
			plot_knn(X, Y)

