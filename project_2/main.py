from stats import display_stats
from score_model import score_model_regression_bin_class, best_regression_bin_class

from LinearRegression import rank_linear_models, show_linear_models
from LogisticRegression import rank_logistic_models, show_logistic_models
from SVM import rank_svm_models, show_svm_models
from KNN import rank_knn_models, show_knn_models

import pandas as pd


def require_int(prompt=None):
	try:
		return int(input(prompt if prompt else 'Enter integer: '))
	except ValueError:
		print('Invalid input, an integer is required.')
		return require_int(prompt)


def get_iterations():
	print('''
Number of times to train the model.  The result will be averaged.  Picking a
large number will take a long time to complete.  Choose below 5 to go fast.
Choose around 200 for bester accuracy.''')

	iterations = require_int()
	if iterations < 1:
		print('Enter a larger number.')
		return get_iterations()

	return iterations

################################################################################

def show_models(test_size, iterations):
	if 'y' == input("Display k-NN? [y/N]").lower():
		show_knn_models(test_size, iterations)

	if 'y' == input("Display SVM? [y/N]").lower():
		show_svm_models(test_size, iterations)

	if 'y' == input("Display Linear? [y/N]").lower():
		show_linear_models(test_size, iterations)

	if 'y' == input("Display Logistic? [y/N]").lower():
		show_logistic_models(test_size, iterations)

################################################################################

def rank_models_splits(test_size_splits, iterations):
	ranks = pd.DataFrame(columns=['title', 'train', 'test', 'test_size'])
	funcs = []

	if 'y' == input("Rank k-NN Models? [y/N]").lower():
		funcs.append(rank_knn_models)

	if 'y' == input("Rank SVM Models? [y/N]").lower():
		funcs.append(rank_svm_models)

	if 'y' == input("Rank Linear Models? [y/N]").lower():
		funcs.append(rank_linear_models)

	if 'y' == input("Rank Logistic Models (slow)? [y/N]").lower():
		funcs.append(rank_logistic_models)

	if len(funcs) == 0:
		print('\n\nPick at least one model class to perform a ranking.')
		return rank_models_splits(test_size_splits, iterations)

	for test_size in test_size_splits:
		for func in funcs:
			ranks = ranks.append(func(test_size, iterations))

	return ranks.sort_values('test', ascending=False).reset_index(drop=True)


def find_best_models(test_size_splits, iterations):
	return rank_models_splits(test_size_splits, iterations).pivot_table(
		values=['train', 'test'],
		index=['title'],
		columns=['test_size']
	).sort_values(('test', 0.3), ascending=False)

#### Main ######################################################################

def main():
	if 'y' == input("> Display statistics? [y/N]").lower():
		display_stats()

	if 'y' == input("> Display models? [y/N]").lower():
		show_models(test_size=0.2, iterations=get_iterations())

	if 'y' == input("> Find best models at different splits? [y/N]").lower():
		results = find_best_models(test_size_splits=[0.1, 0.2, 0.3], iterations=get_iterations())
		print(results.round(2).head(50))
		results.to_csv('results.csv')


if __name__ == '__main__':
	main()

