from sklearn.preprocessing import (
	OneHotEncoder,
	RobustScaler,
)
import pandas as pd


# https://archive.ics.uci.edu/ml/datasets/student+performance
filename = '../datasets/student-mat.csv'
dataset = pd.read_csv(filename, sep=';')

numeric_predictors = [
	'age',
	'Medu',
	'Fedu',
	'traveltime',
	'studytime',
	'failures',
	'famrel',
	'freetime',
	'goout',
	'Dalc',
	'Walc',
	'health',
	'absences',
	'G1',
	'G2',
	#'G3',
]

categorical_predictors = [
	'school',
	'sex',
	'address',
	'famsize',
	'Pstatus',
	'Mjob',
	'Fjob',
	'reason',
	'guardian',
	'schoolsup',
	'famsup',
	'paid',
	'activities',
	'nursery',
	'higher',
	'internet',
	'romantic',
]

################################################################################

response_grade = 'G3'
passing_pct = 0.55

passing = int(dataset[response_grade].max() * passing_pct)
response = 'passed'

dataset = dataset.join(
	(dataset[response_grade] >= passing).rename(response)
)

#### Encode ####################################################################

encoder = OneHotEncoder(drop='first')
encoder.fit(dataset[categorical_predictors])

encoded_categorical_predictors = pd.DataFrame(
	encoder.transform(dataset[categorical_predictors]).toarray(),
	columns=encoder.get_feature_names(),
)

X = dataset[numeric_predictors].join(encoded_categorical_predictors)

#### Scale #####################################################################

transformer = RobustScaler()

X[X.columns] = transformer.fit_transform(X)
Y = dataset[response]

